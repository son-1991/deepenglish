import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Text, FlatList, Dimensions, WebView } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import Video from 'react-native-video';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AudioPlayer from '../components/AudioPlayer';

width = Dimensions.get('window').width;

class MediaLesson extends Component {
    state = {
        lessonComponents: [],
        token: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjozNTE4OSwiaWF0IjoxNTYxMDQwMTYxLCJleHAiOjE1NjExNDAxNjF9.mx2ddYcasTTmr8kgV9CA4INd0-tKKsrjC7BX7dAQI4Q'
    }
    componentWillMount () {
        this.getData();
    }
    componentDidMount () {
        width = Dimensions.get('window').width;
    }
    getData = async() => {
        this.setState({lessonComponents: this.props.lessonComponents});
        // console.log('-- components ---->', this.state.lessonComponents)
    }
    renderLessonComponent = ({item}) => {
        // console.log('-- rendered lesson item ---->', item);
        let videoUrl = 'https://player.vimeo.com/external/228815664.sd.mp4?s=f62c15fb6a688e9fb712b0be7eb155f980636bb8&profile_id=165&oauth2_token_id=1207858618';
        audio = {
            id: 155,
            type: "mp3",
            path: "/TS05/5-04-TS5-Active-Speaking",
            title: "04 - Active Speaking",
            CompanyId: 1,
            url: "https://s3.amazonaws.com/dedownloads/TS05/5-04-TS5-Active-Speaking.mp3?Expires=1560979541&AWSAccessKeyId=AKIAJE2RSSJPUDZFJYOQ&Signature=S4Dd6QWzYnNq4QeI7tMbSyfZZ9M%3D"
        };
        transcriptText = `
        <select class="translate-choose-language full-width yellow js-metabox-translate" placeholder="Choose language ...">
            <option value="" disabled selected>Choose language ...</option><option value=ar>Arabic</option><option value=bg>Bulgarian</option><option value=zh-CN>Chinese (Simplified)</option><option value=hr>Croatian</option><option value=cs>Czech</option><option value=da>Danish</option><option value=nl>Dutch</option><option value=et>Estonian</option><option value=fi>Finnish</option><option value=fr>French</option><option value=de>German</option><option value=el>Greek</option><option value=iw>Hebrew</option><option value=hi>Hindi</option><option value=hu>Hungarian</option><option value=it>Italian</option><option value=ja>Japanese</option><option value=ko>Korean</option><option value=lv>Latvian</option><option value=lt>Lithuanian</option><option value=ms>Malay</option><option value=no>Norwegian</option><option value=fa>Persian</option><option value=pl>Polish</option><option value=pt>Portuguese</option><option value=ro>Romanian</option><option value=ru>Russian</option><option value=sr>Serbian</option><option value=sk>Slovak</option><option value=sl>Slovenian</option><option value=es>Spanish</option><option value=sv>Swedish</option><option value=tl>Tagalog (Filipino)</option><option value=th>Thai</option><option value=tr>Turkish</option><option value=uk>Ukrainian</option><option value=vi>Vietnamese</option>
        </select>
        <p>This is Adam Koch from Central Broadcasting News. I&#8217;m speaking from the roof of our broadcasting office in mid-town New York City.</p>
        <p>Where is he speaking? He is speaking from the roof of the broadcasting office in mid-town New York City.</p>
        <p>The bells you hear are warning everyone to leave the city. The aliens are coming. In the last hour, thousands of people have left New York City.</p>
        <p>Why are people leaving the city? They are leaving the city because the aliens are coming.</p>
        <p>If you are in New York City, stay away from the bridges. The trains are still running, but the street traffic has completely stopped. Our army has been defeated.</p>
        <p>What has happened to the army? The army has been defeated.</p>
        <p>I repeat, the army around New York City has been completely destroyed.</p>
        <p>This may be the last broadcast. We will continue reporting until the end.</p>
        <p>There is a church below us. Many people are running inside the church.</p>
        <p>Where are the people running? They are running inside the church.</p>
        <p>Looking out to the ocean, I can see boats filled with people leaving the city. The streets are full of cars. It&#8217;s so crowded, all the cars have stopped. There are people running everywhere. For our listeners in mid-town, do not try to drive! I repeat, do not try to drive. The street traffic in mid-town has completely stopped.</p>
        <p>Why should people not drive? People shouldn&#8217;t drive because the street traffic in mid-town has completely stopped.</p>
        <p>Wait a minute. There are clouds of smoke coming from the North. It may be a fire or this could be the alien poison gas.</p>
        <p>I see something in the south. It&#8217;s, it&#8217;s the&#8230;</p>
        <p>I believe it&#8217;s the alien ships. Yes, I can see the alien ships coming. There are three, no four, no five alien ships.</p>
        <p>How many alien ships are coming? There are five alien ships coming.</p>
        <p>There are five alien ships flying over the river from the south. They are moving very fast. They are now above downtown. Just a moment ladies and gentlemen. I have just received an emergency report from our Los Angeles office.</p>
        <p>Has he received an emergency report from the New York office? No, he hasn&#8217;t received an emergency report from the New York office. He has received an emergency report from the Los Angeles office.</p>
        <p>Alien ships are landing all over the country. There are two ships outside of Los Angeles. Chicago, Seattle, and Washington DC are also reporting alien ships. I repeat there are ships landing all over the country.</p>
        <p>Are the ships only landing in New York? No, the ships aren&#8217;t only landing in New York. The ships are landing all over the country.</p>
        <p>The whole country is being attacked. This may be the end. <br />Wait a minute. I&#8217;ve just received another report. This is a direct message from the President of the United States.</p>
        <p>Is this a direct message from the President of the United States? Yes, this is a direct message from the President of the United States.</p>
        <p>Citizens of the United States, please remain calm. It is true, all the cities of our great nation are being attacked. We ask all of you to leave the cities now. Do not pack your bags. There is no time. Leave the cities now and remain calm. Do not be afraid. We are in control. We have not been defeated. We will fight these aliens with all our power&#8230;..</p>
        <p>Does that sound like a true story? No, of course, it&#8217;s not. No one would believe that right? Actually, there was a very similar radio show and many people believed it. It was a fictional story, but many people thought it was true.</p>
        <p>Was it a true story? No, it wasn&#8217;t a true story. It was a fictional story.</p>
        <p>The panic was very real. Thousand of Americans believed that aliens were attacking the United States. Many of these people were truly afraid.</p>
        <p>Were many people excited? No, many people weren&#8217;t excited. Many people were afraid.</p>
        <p>The year was 1938. It was ten years before the introduction of television. At this time, radio programs were very popular. News, music, weather reports and dramas were just some of the programs people enjoyed listening to on the radio.</p>
        <p>What kind of radio programs did people enjoy? They enjoyed news, music, weather reports and dramas.</p>
        <p>At that time a young man named Orson Welles had a regular weekly radio program performing dramas. One week, Welles and his team of actors performed a story called The War of the Worlds. The story was from a book also called The War of the Worlds that was written in 1898.</p>
        <p>When was the book written? The book was written in 1898.</p>
        <p>The original story was about an alien attack in England.</p>
        <p>What was the original story about? The original story was about an alien attack in England.</p>
        <p>Orson and his writers changed the story and made it about the United States. At the beginning of the radio program Welles introduced The War of the Worlds as a drama.</p>
        <p>Did Welles tell people that the show was a drama? Yes, at the beginning of the program, Welles told people that the show was a drama.</p>
        <p>The people who heard the beginning of the program knew it wasn&#8217;t real but many people who didn&#8217;t hear the beginning of the show were fooled. The War of the Worlds show sounded like a real news report about aliens.</p>
        <p>Did the show sound like a drama? No, the show didn&#8217;t sound like a drama. The show sounded like a real news report.</p>
        <p>Welles and his team of actors mixed regular music and weather reports with the news reports of the alien attack. This also made it sound very real. No one is sure how many people thought the radio program was a real news program.</p>
        <p>How many people thought the program was real? No one is sure how many people thought the program was real.</p>
        <p>Six million people listened to Welles&#8217; War of the Worlds program. Some people say that one million thought it was real. Other people say that only thousands thought it was real. Some people went to churches. Other people got in their cars and left cities to escape the aliens. Many people called the police to ask for help.</p>
        <p>Why did people call the police? People called the police to ask for help.</p>
        <p>One police station received 2,000 emergency calls in two hours. It was just a drama, but many people panicked.</p>
        <p>It&#8217;s hard to believe isn&#8217;t it? How could people think that aliens were attacking? Well, it wasn&#8217;t the only time. In 1949, a man named Leonardo Paez heard the story and loved it. Paez worked for a radio station in Ecuador.</p>
        <p>Where did Paez work? Paez worked for a radio station in Ecuador.</p>
        <p>He decided to copy the show. Paez changed the story to include</p>
        <p>places and people in Ecuador. He wanted the story to fool everyone so he did even more than Welles.</p>
        <p>His radio company also owned a newspaper company. Paez asked the newspaper to write stories about strange UFOs in the days before his radio show.</p>
        <p>What did Paez ask the newspaper to write? Paez asked the newspaper to write stories about strange UFOs.</p>
        <p>Just like Welles, his radio program told a story about a local alien attack that sounded like radio news.</p>
        <p>Just like in the United States people panicked. Many people ran into the streets, but they didn&#8217;t know what to do. Even the army and police were fooled.</p>
        <p>Did the army and police know it was just a radio drama? No, the army and police didn&#8217;t know it was just a radio program. The army and police were fooled.</p>
        <p>Many trucks full of soldiers and police went to the north to look for the aliens. When the people in the streets saw the trucks full of real soldiers, they really believed the aliens were real.</p>
        <p>Many people went to church and prayed to God.</p>
        <p>When people found it was just a fictional story, they were angry.</p>
        <p>How did everyone feel? They were angry.</p>
        <p>Hundreds of people went to the radio and newspaper company building. They threw stones at the building and broke windows. Then they broke the door down. They went inside and broke the printing machines.</p>
        <p>What did they break inside? They broke the printing machines.</p>
        <p>They set fire to the building. There were 50 company employees in the building. They called the fire department and the police, but no one answered. The police and firefighters all went to the north to fight aliens, so no one could help them. Most of the company employees ran to the rooftop and jumped to another building. Leonardo Paez also escaped but his nephew, his girlfriend and 4 others died in the fire.</p>
        <p>Did Paez die? No, Paez didn&#8217;t die. He escaped.</p>
        <p>Paez left the country and never returned to Ecuador.</p>
        <p>What did he do after he escaped? He left the country and never returned to Ecuador.</p>
        <p>It was a terrible end for Paez. Orson Welles was much luckier.</p>
        <p>Was it a terrible end for Welles? No, it wasn&#8217;t a terrible end for Welles. It was a terrible end for Paez.</p>
        <p>Many people were angry with Orson Welles, but no one died and no one was hurt.</p>
        <p>Why was Welles luckier than Paez? Welles was luckier than Paez because no one died and no one was hurt.</p>
        <p>So, why did he do it? Why did he fool thousands into panicking? After Welles&#8217; War of the Worlds show, the police came and questioned everyone. Welles said he didn&#8217;t want to fool anyone. He thought that everyone would know it was a fictional story.</p>
        <p>What did Welles say after the show? He said he didn&#8217;t want to fool anyone.</p>
        <p>Sixteen years later, Welles changed his story. He said he wanted people to be fooled.</p>
        <p>What did he say sixteen years later? Sixteen years later, he said he wanted people to be fooled.</p>
        <p>He wanted to teach everyone a lesson. He thought people were too easily fooled. He wanted people to think more.</p>
        <p>Why did Welles fool everyone? He fooled everyone because he wanted them to think more.</p>
        <p>He wanted everyone to know that they cannot trust information only because it&#8217;s on the radio. Many people think Welles has failed. He hasn&#8217;t taught us anything. People are still easily fooled. Nothing has changed.</p>
        <p>Has anything changed? No, nothing has changed. People are still easily fooled.</p>
        <p>Today, many people believe anything they see on the TV. Welles may have failed to make people think more about what is real and what is fiction but he continued to be very successful. The War of the Worlds was just the beginning for Welles.</p>
        <p>Later, Welles became one of the most famous movie directors of all time.</p>
        <p>What happened to Welles? He became one of the most famous movie directors of all time.</p>
        `;
        if(item.name === 'VimeoEmbed') {
            axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/vimeo/228815664', headers: {Authorization: this.state.token}}).then(result => {
                videoUrl = result.data;
                // console.log('-- videoURL ---->', videoUrl);
            }, error => {
                console.error(error);
            })
            return (
                <Video 
                        source={{uri: videoUrl}}
                        controls={true}
                        paused={true}
                        fullscreenAutorotate={true}
                        style={{width: width, height: width/2, backgroundColor: '#fff'}}/>
            )
        }
        if(item.name === 'AudioPlayer') {
            axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/mediasigned/' + item.asset_id, headers: {Authorization: this.state.token}}).then(result => {
                audio = result.data.media;
                // console.log('-- audio ---->', audio);
            }, error => {
                console.error(error);
            })
            return (
                <View>
                <Text style={{textAlign: 'center', width: '100%'}}>{audio.title}</Text>
                <AudioPlayer free={false} uri={audio.url}/>
                </View>
            )
        }
        if (item.name === 'DownloadButton') {
            return (
                <TouchableOpacity style={{alignItems: 'center', paddingBottom: 10}}onPress = { () => this.downloadMedia }><Text style={{textAlign: 'center', backgroundColor: '#2a5477', color:'#fff', padding: 5}}>{item.label}</Text></TouchableOpacity>
            )
        }
        if (item.name === 'Html') {
            return (
                <Text style={{fontSize: 18, padding: 20, width: '100%', color: '#232323', textAlign:'center'}}>{item.content}</Text>
            )
        }
        if (item.name === 'TranscriptBox') {
            axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/transcript/'+item.transcript_id, headers: {Authorization: this.state.token}}).then(result => {
                transcriptText = result.data.transcript[0].content.content;
                console.log('-- text ---->',transcriptText);
            }, error => {
                console.error(error);
            })
            return (
                <View style={{height: Dimensions.get('window').width, width: '100%'}}>
                    <WebView
                        source={{html: transcriptText}}
                    />
                </View>
            )
        }
        return (<View></View>)
    }
    render () {
        return(
            <FlatList
                data = {this.state.lessonComponents}
                renderItem = {this.renderLessonComponent}
                keyExtractor = {(item, index) => index.toString()}
            />
        )
    }
}

export default class AuthuserLessonset extends Component {

    state = {
        token: '',
        index: 0,
        routes: [],
        currentLesson: {}, //item, 
        lessons: {}, //this.state.lessons, 
        lessonset: {}, //this.state.lessonset
        videoUrl: 'https://player.vimeo.com/external/228815664.sd.mp4?s=f62c15fb6a688e9fb712b0be7eb155f980636bb8&profile_id=165&oauth2_token_id=1207858618',
        audio: {},
        userQuiz: [],
        lessonQuiz: []
    }

    componentWillMount() {
        this.getData();
    }
    getData = async () => {
        this.setState({currentLesson: this.props.navigation.state.params.currentLesson, lessons: this.props.navigation.state.params.lessons, lessonset: this.props.navigation.state.params.lessonset});
        this.setState({token: await AsyncStorage.getItem('@authToken')});
        route = [
            { key: 'first', title: 'LESSON' },
            { key: 'second', title: 'RESOURCES' }
        ];
        if(this.state.currentLesson.QuizId)
            route.push({key: 'third', title: 'QUIZ'})
        this.setState({routes: route})
    }
    
    LessonScreen = () => {
        console.log('lessonsetscreen');
        lsComponents = [];
        l = 0;
        for (i = 0; i < this.state.currentLesson.content.rows.length; i ++)
            for (j = 0; j < this.state.currentLesson.content.rows[i].columns.length; j ++)
                lsComponents[l++] = this.state.currentLesson.content.rows[i].columns[j].component;
        return (
            <View>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#909090'}}>{this.state.currentLesson.title}</Text>
                <MediaLesson lessonComponents = {lsComponents}/>
            </View>
        );
    };

    renderResourceComponent = ({ item }) => {
        const { navigate } = this.props.navigation;
        console.log('item-resource', item);
        return (
            <TouchableOpacity  onPress={() => { navigate('MembersLesson', {currentLesson: item, lessons: this.state.lessons, lessonset: this.state.lessonset}) }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }
      
    ResourcesScreen = () => {
        console.log('Resourcescreen');
        return(
            <View>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#909090'}}>Lesson Resources</Text>
                <FlatList
                    data = {this.props.navigation.state.params.lessonset.resources.resources}
                    renderItem = {this.renderResourceComponent}
                    keyExtractor = {(item, index) => index.toString()}
                />
            </View>
        )
    };

    QuizScreen = () => {
        console.log('QuizScreen');

        axios({method:'GET', 'url': 'https://api.deepdev.xyz/api/userquiz/'+ this.state.currentLesson.QuizId, headers: {Authorization: this.state.token}}).then(result=> {
            this.setState({userQuiz: result.data.quiz[0]});
        }, error => {
            console.error(error);
        })

        axios({method:'GET', 'url': 'https://api.deepdev.xyz/api/quiz/'+ this.state.currentLesson.QuizId, headers: {Authorization: this.state.token}}).then(result=> {
            this.setState({lessonQuiz: result.data.quiz[0]});
            console.log('userquiz-->', result.data.quiz[0])
        }, error => {
            console.error(error);
        })

        return (
            <View>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#909090'}}>Quiz Screen</Text>
                                                                                                        {/* {this.state.userQuiz.title} */}
                
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>                
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: this.LessonScreen,
                        second: this.ResourcesScreen,
                        third: this.QuizScreen,
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    tabBarPosition='bottom'
                    lazy={true}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    activityIndicator: {
        padding: 25
    },
})
