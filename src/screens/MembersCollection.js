import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Text, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export default class MembersCollection extends Component {

    constructor(props) {
        super(props);
        
        state = {
            userInfo: {},
            token: '',
            collection: {},
            lessonsets: {},
            authuser_plans: [],
            collectionId: -1,
            isLoading: false
        };
    }

    componentWillMount() {
        console.log('------ Members Collection -----')
        this.setState({collectionId: this.props.navigation.state.params.collectionId});
        this.setState({isLoading: true});
        this.getCollectionData();
        this.setState({isLoading: false});
    }

    getCollectionData = async () => {
        //get user token
        this.setState({token: await AsyncStorage.getItem('@authToken')});
        console.log('-- token ---->', this.state.token)
        //get user plan
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/getuserplans/', headers: {Authorization: this.state.token}}).then(result => {
             this.setState({authuser_plans: result.data.plans});
        }, error => {
            console.error(error);
        });
        //get lessonsets
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/lessonsets/' + this.state.collectionId, headers: {Authorization: this.state.token}}).then(result => {
             this.setState({lessonsets: result.data.lessonsets});
        }, error => {
            console.error(error);
        });

        //get collection
        let collections = [];
        for(let i = 0; i < this.state.authuser_plans.length; i ++ )
            for(let j = 0; j < this.state.authuser_plans[i].Product.Collections.length; j ++)
                collections.push(this.state.authuser_plans[i].Product.Collections[j]);
        collectios = collections.filter((item) => item.id === this.state.collectionId);
        this.setState({collection: collections[0]});
        console.log('-- collection ---->', this.state.collection);
    }

    renderLessonItem = ({item}) => {
        return(
            <TouchableOpacity 
                onPress={() => {
                    this.props.navigation.navigate('LessonSet', {collectionId: this.state.collectionId, id: item.id});
                }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        console.log('-- rendering ---->')
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#909090'}}>
                True Stories Fluency</Text>
                 {/* this.state.collection.name */}
                 {/* True Stories Fluency */}
                <FlatList
                    data = {this.state.lessonsets}
                    renderItem = {this.renderLessonItem}
                    keyExtractor = {item => item.id.toString()}
                    // ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    activityIndicator: {
        padding: 25
    },
})
