import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator, Text, FlatList, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

export default class FavoriteLessons extends Component {

    state = {
        loading: false,
        favlessons: [],
    }

    componentDidMount() {
        this.getFavoriteLessons();
    }
    
    getFavoriteLessons = async () => {
        this.setState({loading: true});
        token = await AsyncStorage.getItem('@authToken');
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/bookmarks/', headers: {Authorization: token}}).then(result => {
             this.setState({favlessons: result.data});
             console.log('favlessons',this.state.favlessons);
        }, error => {
            console.error(error);
        });
        this.setState({loading: false});
    }

    renderItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        return (
            <TouchableOpacity  onPress={() => { 
                console.log('selected item', item), navigate('FavoriteLessonsDetail', {lesson: item})
            }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.data.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <FlatList
                    data = {this.state.favlessons}
                    renderItem = {this.renderItem}
                    keyExtractor = {item => item.id.toString()}
                    ListFooterComponent =  {() => { 
                        if(this.state.loading) return <ActivityIndicator animating size='large' style={{marginTop: 10}}/>
                        else return <></>
                    }}
                />
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    lessonItem: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 3,
    },
    lessonLogo: {
        height: 80, 
        width: 80, 
        borderRadius: 15, 
        margin: 10
    },
    lessonTitleContent: {
        flex: 1,
        justifyContent: 'center', 
        marginLeft: 5
    },
    lessonTitle: {
        fontWeight: 'bold',
        fontSize: 19,
        color: '#333',
        paddingBottom: 0,
        marginBottom: 0
    },
    applogo: {
        flex: 1,
        alignItems: 'center',
        padding: 0,
        justifyContent: 'center'
    },
    activityIndicator: {
        padding: 25
    },
})
