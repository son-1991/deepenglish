import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator, Text, FlatList, TouchableOpacity } from 'react-native';
import axios from 'axios';

export default class FreeLesson extends Component {

    state = {
        loading: false,
        lessons: [],
        page: 1,
        seed: 1,
        error: null,
        refreshing: false
    }

    componentDidMount() {
        this.getFreeLessons();
    }
    
    getFreeLessons = async () => {
        const { page, seed } = this.state;
        this.setState({loading: true});
        await axios({ method: 'GET', 'url': 'https://deepenglish.com/wp-json/wp/v2/posts?per_page=10&page='+page}).then(result => {
            this.setState(state => ({lessons: [...state.lessons, ...result.data], loading: false}));
        }, error => {
            console.error(error);
        });
    }

    renderItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        return (
            <TouchableOpacity  onPress={() => { console.log('selected item', item), navigate('FreeLessonDetail', {lesson: item}) }}>
                <View style={styles.lessonItem}>
                    <Image 
                        style={styles.lessonLogo}
                        source={{uri: item.better_featured_image.media_details.sizes.thumbnail.source_url}}
                        />
                    <View style={styles.lessonTitleContent}>
                        <Text
                            style={styles.lessonTitle}
                        >{item.title.rendered}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    handleEnd = () => {
        this.setState(state => ({page: state.page + 1}), () => this.getFreeLessons());
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <FlatList
                    data = {this.state.lessons}
                    renderItem = {this.renderItem}
                    keyExtractor = {item => item.id.toString()}
                    onEndReached = { this.handleEnd }
                    onEndReachedThreshold = {0}
                    ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                />
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    lessonItem: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 3,
    },
    lessonLogo: {
        height: 80, 
        width: 80, 
        borderRadius: 15, 
        margin: 10
    },
    lessonTitleContent: {
        flex: 1,
        justifyContent: 'center', 
        marginLeft: 5
    },
    lessonTitle: {
        fontWeight: 'bold',
        fontSize: 19,
        color: '#333',
        paddingBottom: 0,
        marginBottom: 0
    },
    applogo: {
        flex: 1,
        alignItems: 'center',
        padding: 0,
        justifyContent: 'center'
    },
    activityIndicator: {
        padding: 25
    },
})
