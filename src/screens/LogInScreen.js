import React, { Component } from 'react';
import { StyleSheet, View, Image, TextInput, Text, Alert, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { StackActions, NavigationActions } from 'react-navigation';

const logo = require('../assets/images/logo.png')

export default class AuthScreen extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        email: '',
        password: '',
        confirmPassword: '',
        isLoggingIn: true,
    }

    componentDidMount() {

    }

    onLogin = async () => {
        const { navigate } = this.props.navigation;
        result = await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/users/login', data: {email: this.state.email, password: this.state.password }})
        console.log('token ---',result.data);
        await AsyncStorage.setItem('@authToken', result.data.token);
        navigate('FreeLesson');
    }

    onRegister = async () => {
        if (this.state.password !== this.state.confirmPassword) {
            Alert.alert('Your poasswords do not match.');
            return;
        }

    }
    
    onSubmit = () => {
        if (!this.state.email || !this.state.password) {
            Alert.alert('Please provide both an email address and password.');
            return;
        }
        if (this.state.isLoggingIn) {
            this.onLogin();
        } else {
            this.onRegister();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.applogo}>
                    <Image resizeMode="contain" style={{height: 70}} source={logo} />
                </View>
                <View style={styles.form}>
                    <View style={styles.textInput}>
                        <TextInput
                            placeholder="Email"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            onChangeText={(email) => this.setState({email})}/>
                    </View>
                    <View style={styles.textInput}>
                        <TextInput
                            placeholder="Password"
                            secureTextEntry={true}
                            onChangeText={(password) => this.setState({password})}/>
                    </View>
                    {   
                        !this.state.isLoggingIn && 
                        <View style={styles.textInput}>
                            <TextInput
                                placeholder="Confirm Password"
                                secureTextEntry={true}
                                onChangeText={(confirmPassword) => this.setState({confirmPassword})}/>
                        </View>
                    }
                    <TouchableOpacity onPress = { () => this.onSubmit() } style={styles.submitButton}>
                        <Text>{this.state.isLoggingIn ? 'LOG IN' : 'SIGN UP'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    page: {
        alignItems: 'center',
        flex: 1
    },
    form: {
        width: Dimensions.get('window').width - 60,
        justifyContent: 'center'
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#d2d2d2',
        color: '#999999',
        paddingBottom: 10,
        paddingTop: 30,
    },
    submitButton: {
        borderRadius: 5,
        backgroundColor: '#f1c40f',
        color: '#232323',
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 15,
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 35
    },
    applogo: {
        paddingBottom: 30,
        height: 70,
        fontWeight: 'bold',
    }
})
