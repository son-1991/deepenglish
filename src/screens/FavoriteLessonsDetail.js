import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Text, ImageBackground, WebView, ScrollView, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import AudioPlayer from '../components/AudioPlayer';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const favChecked = require('../assets/images/favorite-checked.png');
const favUnchecked = require('../assets/images/favorite-unchecked.png');
const close = require('../assets/images/close.png');

export default class FreeLessonDetail extends Component {

    constructor(props) {
        super(props);
        
        state = {
            lesson: {},
            isBookmarked: false,
            text: '',
            token: ''
        };
    }

    componentWillMount() {
        this.setState({lesson: this.props.navigation.state.params.lesson});
        this.getUserInfo();

        webText = this.props.navigation.state.params.lesson.content.rendered;
        webText = webText.replace(/<span[^>]*>([^<]+)<\/span>/g,'$1');
        webText = webText.replace(/<p[^>]*>([^<]+)<\/p>/g, '$1');
        this.setState({text: webText});
    }

    getUserInfo = async () => {
        //get user token
        this.setState({token: await AsyncStorage.getItem('@authToken')});
    }

    componentDidMount() {
        
        // slowSound = new Sound('https://deepenglish.com/audio/' + this.state.lesson.CFS.url_slow, null, (error) => {
        //     if (error) {
        //         console.error(error)
        //     }
        // });

        // mediumSound = new Sound('https://deepenglish.com/audio/' + this.state.lesson.CFS.url_debate, null, (error) => {
        //     if (error) {
        //         console.error(error)
        //     }
        // });

        // fastSound = new Sound('https://deepenglish.com/audio/' + this.state.lesson.CFS.url_fast, null, (error) => {
        //     if (error) {
        //         console.error(error)
        //     }
        // });
    }

    // state = {
    //     loading: false,
    //     lessons: {},
    //     refreshing: false,
    //     userInfo: {},
    //     image: 'https://deapp.imgix.net/contentmeta/Core_Audio.png?w=288&h=181&s=6aeae2d1ad6ee73eda3b9681c6546d9d'
    // }

    // componentWillMount() {
    //     this.getLessonItemDetail();
    // }
    
    // async getLessonItemDetail() {
    //     const { page, seed } = this.state;
    //     await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/users/login', data: {email:'blogtrai@gmail.com', password: 'de123start*' }}).then(result => {
    //         this.setState({userInfo: result.data});
    //         console.log('userInfo', this.state.userInfo);
    //     }, error => {
    //         console.error(error);
    //     });
    //     await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/lessonsetslimited/'+ this.state.userInfo.user.Companies[0].id, headers: {Authorization: this.state.token}}).then(result => {
    //          this.setState({lessons: result.data});
    //          console.log('lessons', this.state.lessons);
    //     }, error => {
    //         console.error(error);
    //     });

    //     if( this.state.lessons.lessonsets[0].image !== '') {
    //         let client = new ImgixClient({
    //             domains: 'deapp.imgix.net',
    //             secureURLToken: '5576NB5NbYnwzCFU'
    //         });
    //         options={W: 420, h: 210, fit: 'clamp', auto: 'compress'};
    //         this.setState({image: client.buildURL(this.state.lessons.lessonsets[0].image, options)});
    //     }
    //     // await axios({method: 'GET', 'url': this.state.image}).then( result => {
    //     //     this.setState({image: result.data});
    //     // }, error => {
    //     //     console.error(error);
    //     // });
    //     console.log('image-url',this.state.image);
    // }

    // formatDescription(content) {
    //     let string = content.replace(/(<([^>]+)>)/ig,'');
    //     let length = 100;
    //     let trimmedString = string.length > length ? string.substring(0, length - 3) + '...' : string.substring(0, length);
    //     return trimmedString;
    // }

    onBookmark = async () => {
        this.setState({isBookmarked: !this.state.isBookmarked});

        if(this.state.isBookmarked) {
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/addbookmark/', data: {id: this.state.lesson.id, type: 'blog', title: this.state.lesson.title.rendered}, headers: {Authorization: this.state.token}}).then(result => {
                console.log('AddBookmark');
            }, error => {
                console.error(error);
            });
        } else {
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/removebookmark/', data: {id: this.state.lesson.id, type: 'blog', title: this.state.lesson.title.rendered}, headers: {Authorization: this.state.token}}).then(result => {
                console.log('RemoveBookmark');
            }, error => {
                console.error(error);
            });
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{width:'100%', height: width/4 }}>
                    <ImageBackground source={{uri: this.state.lesson.better_featured_image.media_details.sizes.medium_large.source_url}} style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}>
                        <View style={{flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                            <TouchableOpacity style={{margin: 5}} onPress={this.onBookmark}>
                                <Image style={{width: 25, height: 25}} source={this.state.isBookmarked ? favChecked : favUnchecked}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{margin: 5}} onPress={() => this.props.navigation.goBack(null)}>
                                <Image style={{width: 25, height: 25}} source={close}/>
                            </TouchableOpacity>
                        </View>
                        <Text style={{fontSize: 20, color: '#fff', backgroundColor: '#4f8ab6', opacity: 0.8, width: '100%', textAlign: 'center', marginBottom: 5}}>{this.state.lesson.title.rendered}</Text>
                    </ImageBackground>
                </View>
                <View style={{width:'100%', height: width/3}}>
                    <AudioPlayer title="Slow" uri={this.state.lesson.CFS.url_slow} />
                    <AudioPlayer title="Normal" uri={this.state.lesson.CFS.url_debate} />
                    <AudioPlayer title="Fast" uri={this.state.lesson.CFS.url_fast} />
                </View>
                <View style={{ width:'100%', height: (height - width * 7 / 12), fontSize: 15 }}>
                    <WebView
                        source={{html: this.state.text}}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    lessonItem: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 3,
    },
    lessonLogo: {
        height: 80, 
        width: 80, 
        borderRadius: 15, 
        margin: 10
    },
    lessonTitleContent: {
        flex: 1,
        justifyContent: 'center', 
        marginLeft: 5
    },
    lessonTitle: {
        fontWeight: 'bold',
        fontSize: 19,
        color: '#333',
        paddingBottom: 0,
        marginBottom: 0
    },
    applogo: {
        flex: 1,
        alignItems: 'center',
        padding: 0,
        justifyContent: 'center'
    },
    activityIndicator: {
        padding: 25
    },
})
