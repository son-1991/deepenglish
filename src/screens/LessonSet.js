import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator, Text, FlatList, Dimensions, TouchableOpacity, ImageBackground } from 'react-native';
import Button from 'react-native-button';
import { TabView, SceneMap } from 'react-native-tab-view';
import axios from 'axios';
import ImgixClient from 'imgix-core-js';
import AsyncStorage from '@react-native-community/async-storage';

const favChecked = require('../assets/images/favorite-checked.png');
const favUnchecked = require('../assets/images/favorite-unchecked.png');

export default class AuthuserLessonset extends Component {

    state = {
        token: '',
        index: 0,
        routes: [
            { key: 'first', title: 'LESSON SET' },
            { key: 'second', title: 'RESOURCES' },
        ],
        loading: false,
        lessonset: {},
        lessons: [],
        lessonResource: [],

        id: -1,
        collectionId: -1,
        image: 'https://deapp.imgix.net/contentmeta/Core_Audio.png?w=288&h-181&s=6aeae2d1ad6ee73eda3b9681c6546d9d',
        isBookmarked: false,
        alreadyComplete: false,
        
    }

    componentWillMount() {
        console.log('----- Lesson Set -----')
        navigate = this.props.navigation.state.params;
        this.setState({id: navigate.id, collectionId: navigate.collectionId});

        this.setState({loading: true});
        this.getData();
        this.setState({loading: false});

        console.log('state --- ', this.state);
    }
    
    getData = async () => {
        //get user token
        this.setState({token: await AsyncStorage.getItem('@authToken')});
        //get lessonsets
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/lessonsets/' + this.state.collectionId, headers: {Authorization: this.state.token}}).then(result => {
            this.setState({lessonset: result.data.lessonsets.filter(lessonset => {return lessonset.id === this.state.id})});
            this.setState({lessonset: this.state.lessonset[0]});
            this.setState({lessonResource: this.state.lessonset.resources.resources})
            console.log('-- lesson set ---->', this.state.lessonset)
        }, error => {
            console.error(error);
        });

        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/lessonsinset/' + this.state.lessonset.id, headers: {Authorization: this.state.token}}).then(result => {
            this.setState({lessons: result.data.lessons});
        }, error => {
            console.error(error);
        });

        //get background image url
        let client = new ImgixClient({
            domains: 'deapp.imgix.net',
            secureURLToken: '5576NB5NbYnwzCFU'
        });
        options={W: 420, h: 210, fit: 'clamp'};
        options.auto = 'compress';
        this.setState({image: client.buildURL(this.state.lessonset.image, options)});

        //get bookmarked and completed state
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/bookmarks/', headers: {Authorization: this.state.token}}).then(result => {
            bookmarks = result.data;
            bookmarks = bookmarks.filter(item => {
                return item.data.id === this.state.lessonset.id
            })
            console.log('-- bookmark info ---->', bookmarks)
            this.setState({isBookmarked: bookmarks.length ? true : false})
        }, error => {
            console.error(error);
        });

        // await axios({ method: 'GET', 'url': 'https://api/deepdev.xyz/api/track', headers: {Authorization: this.state.token}, params: {'section': 'LessonSet', 'sectionId': this.state.id}}).then(result => {
        //     tracks
        // })
    }

    renderLessonItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        // console.log('item-lesson', item);
        return (
            <TouchableOpacity  onPress={() => { navigate('MembersLesson', {currentLesson: item, lessons: this.state.lessons, lessonset: this.state.lessonset}) }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderResourceItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        // console.log('item-resource', item)
        return (
            <TouchableOpacity  onPress={() => { navigate('FreeLessonDetail', {lesson: item}) }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    onBookmark = async () => {
        this.setState({isBookmarked: !this.state.isBookmarked});

        if(this.state.isBookmarked) {
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/addbookmark/', data: {id: this.state.lessonset.id, type: 'blog', title: this.state.lessonset.name}, headers: {Authorization: this.state.token}}).then(result => {
                //console.log('AddBookmark');
            }, error => {
                console.error(error);
            });
        } else {
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/removebookmark/', data: {id: this.state.lessonset.id, type: 'blog', title: this.state.lessonset.name}, headers: {Authorization: this.state.token}}).then(result => {
                //console.log('RemoveBookmark');
            }, error => {
                console.error(error);
            });
        }
    }

    checkComplete = async() => {
        userPlans = [];
        products = [];
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/getuserplans/', headers : {Authorization: this.state.token}}).then(result => {
            userPlans = result.data.plans;
        }, error => {
            console.error(error);
        })
        userPlans.forEach(plan => {
            products.push(plan.Product);
        });

        collection = [];
        products.forEach(item => {
            collection = collection.concat(item.Collections.filter(obj => {
                return obj.id === this.state.collectionId
            }))
        });

        console.log('-- collection ---->', collection)

        product = products.filter(item => {
            return item.id === collection[0].ProductCollection.ProductId
        })

        console.log('-- product ---->', product)
        console.log('-- sectionID ---->', this.state.id);
        console.log('-- productID ---->', product[0].id);
        !this.state.alreadyComplete ?
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/track/remove', data: {section: 'LessonSet', sectionId: this.state.id, productId: product[0].id}, headers: {Authorization: this.state.token}}).then(result => {
                console.log(' ---1 ---->', result.data)
            }, error => {
                console.error(error)
            }) :
            await axios({ method: 'POST', 'url': 'https://api.deepdev.xyz/api/track', data: {section: 'LessonSet', sectionId: this.state.id, productId: product[0].id, data: {status: 1}}, headers: {Authorization: this.state.token}}).then(result => {
                console.log(' ---2 ---->', result.data)
            }, error => {
                console.error(error)
            })
    }

    onAlreadyComplete = () => {
        this.checkComplete();
        this.setState({alreadyComplete: !this.state.alreadyComplete});
    }

    LessonSetScreen = () => {
        console.log('-- lessonsetscreen ---->', this.state.lessons);
        return (
            <View style={{height: Dimensions.get('window').width}}>
                <FlatList
                    data = {this.state.lessons}
                    renderItem = {this.renderLessonItem}
                    keyExtractor = {item => item.id.toString()}
                    // ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                />
            </View>
    )};
      
    ResourcesScreen = () => {
        console.log('-- Resourcescreen ---->', this.state.lessonResource);
        return (
            <View>
                <FlatList
                    data = {this.state.lessonResource}
                    renderItem = {this.renderResourceItem}
                    keyExtractor = {item => item.name}
                    // ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                />
            </View>
    )};

    render() {
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#232323'}}>{this.state.lessonset.name}</Text>
                <View style={{width: width, height: width/2}}>
                    <ImageBackground source={{uri: this.state.image}} style={{flex: 1}}>
                        <TouchableOpacity style={{margin: 5}} onPress={this.onBookmark}>
                            <Image style={{width: 25, height: 25}} source={this.state.isBookmarked ? favChecked : favUnchecked}/>
                        </TouchableOpacity>
                        <View style={{flex: 1, width: '100%', alignItems:'flex-end', borderWidth: 0, justifyContent:'flex-end', paddingRight: 15, paddingBottom: 10}}>
                            <Button 
                                containerStyle={{backgroundColor: (this.state.alreadyComplete ? '#fff' : '#3AB567')}}
                                onPress={() => this.onAlreadyComplete()} >
                                <Text style={{fontSize: 15, color: (this.state.alreadyComplete ? '#333' : '#fff'), marginVertical: 10, marginHorizontal: 5}}>{this.state.alreadyComplete ? 'COMPLTETED' : 'MARK AS COMPLETED'}</Text>
                            </Button>
                        </View>
                    </ImageBackground>
                </View>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: this.LessonSetScreen,
                        second: this.ResourcesScreen,
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    tabBarPosition='bottom'
                    lazy={true}
                />
                {/* <FlatList
                    data = {this.state.lessons}
                    renderItem = {this.renderLessonItem}
                    keyExtractor = {item => item.id.toString()}
                    // ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                /> */}
                {/* <FlatList
                    data = {this.state.lessonResource}
                    renderItem = {this.renderResourceItem}
                    keyExtractor = {item => item.name}
                    // ListFooterComponent =  {() => <ActivityIndicator animating size='large' style={{marginTop: 10}}/>}
                /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    activityIndicator: {
        padding: 25
    },
})
