import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, FlatList, Linking, TouchableOpacity } from 'react-native';

export default class MembersProduct extends Component {

    constructor(props) {
        super(props);
        
        state = {
            product: {},
        };
    }

    componentWillMount() {
        console.log('------- Members Product -------')
        this.setState({product: this.props.navigation.state.params.product});
    }

    renderContentItem = ({item}) => {
        return (
            <TouchableOpacity  onPress={() => {
                if (item.key === 1001)
                    this.props.navigation.navigate('MembersCollection', {collectionId: 3}) 
                if (item.key === 1002)
                    Linking.openURL(item.link)
                if (item.key === 1003)
                    Linking.openURL('https://members.deepenglish.com/connect')
            }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.label}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderCollectionItem = ({item}) => {
        return(
            <TouchableOpacity style={{display : item.ProductCollection.productHomeVisibility ? 'flex' : 'none' }} 
                onPress={() => {
                    console.log('item.id', item.id)
                    this.props.navigation.navigate('MembersCollection', {collectionId: item.id})
                }}>
                <View style={{padding: 15, fontSize: 22, color: '#232323', borderBottomWidth: 1, borderBottomColor: '#d2d2d2'}}>
                    <Text>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#232323'}}>{this.state.product.name}</Text>
                <FlatList
                    data = {this.state.product.Collections}
                    renderItem = {this.renderCollectionItem}
                    keyExtractor = {item => item.id.toString()}
                    style={{height: 0}}
                />
                <Text style={{textAlign: 'center', width: '100%', padding: 20, fontSize: 25, color: '#232323'}}>Resources</Text>
                <FlatList
                    data = {this.state.product.layout.sections[1].content.items}
                    renderItem = {this.renderContentItem}
                    keyExtractor = {item => item.key.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    activityIndicator: {
        padding: 25
    },
})
