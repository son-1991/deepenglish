import React, { Component } from 'react';
import { StyleSheet, View, Image, Dimensions, StatusBar } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

const logo = require('../assets/images/logo.png')

var width = Dimensions.get('window').width;

const homeAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'AppNavigator' })],
});

export class SplashScreen extends Component {
    async componentDidMount() {
        setTimeout(() => {
            this.props.navigation.dispatch(homeAction);
        }, 1500
        );
    }
    static navigationOptions = {
        header: null
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={'#d2d2d2'}
                />
                <View style={styles.applogo}>
                    <Image resizeMode="contain" style={{ width: width * 0.85 }} source={logo} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    applogo: {
        flex: 1,
        alignItems: 'center',
        padding: 0,
        justifyContent: 'center'
    }
})

export default SplashScreen;