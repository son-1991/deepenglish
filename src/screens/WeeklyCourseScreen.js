import React, { Component } from 'react';
import { Linking, View } from 'react-native';

export class WeeklyCourseScreen extends React.Component {
    constructor(props) {
        super(props);
        Linking.openURL('https://deepenglish.com');
        const { navigate } = this.props.navigation;
        navigate('FreeLesson');
    }
    render() {
        return (
            <View></View>
        );
    }
}

export default WeeklyCourseScreen;