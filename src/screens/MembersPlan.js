import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator, Text, FlatList, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

import FreeLessonScreen from './FreeLessons';

const temp = require('../assets/images/translate-icon.png')

export default class MembersPlan extends Component {

    state = {
        authuserPlans: [],
    }

    componentDidMount() {
        console.log('----- Members Plan -----')
        this.getProducts();
    }
    
    getProducts = async () => {
        token = await AsyncStorage.getItem('@authToken');
        await axios({ method: 'GET', 'url': 'https://api.deepdev.xyz/api/getuserplans/', headers: {Authorization: token}}).then(result => {
             this.setState({authuserPlans: result.data.plans});
        }, error => {
            console.error(error);
        });
    }

    renderItem = ({ item }) => {
        const { navigate } = this.props.navigation;
        return (
            <TouchableOpacity  onPress={() => {
                    if(item.Product.id === 2)
                        navigate('MembersProduct', {product: item.Product})
                    if(item.Product.id === 1)
                        navigate('MembersCollection', {collectionId: 4})
                }}>
                <View style={styles.productItem}>
                    <Image 
                        style={styles.productLogo}
                        source={temp}
                        />
                    <View style={{flex: 1, justifyContent:'center'}}>
                        <View style={styles.productTitleContent}>
                            <Text
                                style={styles.productTitle}
                            >{item.Product.name}</Text>
                        </View>
                        <View style={{margin: 5}}>
                            <Text>{item.Product.tagline}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <FlatList
                    data = {this.state.authuserPlans}
                    renderItem = {this.renderItem}
                    keyExtractor = {item => item.id.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#d2d2d2',
        flex: 1,
        justifyContent: 'center',
        marginTop: 120
    },
    productItem: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 3,
        marginLeft: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#d2d2d2'
    },
    productLogo: {
        height: 80, 
        width: 80, 
        borderRadius: 15, 
        margin: 10,
        marginLeft: 0
    },
    productTitleContent: {
        marginLeft: 5
    },
    productTitle: {
        fontWeight: 'bold',
        fontSize: 19,
        color: '#333',
        paddingBottom: 0,
        marginBottom: 0
    },
})
