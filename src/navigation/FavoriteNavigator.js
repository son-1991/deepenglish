import React, {Component} from 'react';
import { createStackNavigator } from 'react-navigation';
import { View, Image, TouchableOpacity } from 'react-native';

import FavoriteLessons  from '../screens/FavoriteLessons';
import FreeLessonDetail from '../screens/FreeLessonDetail';
import FavoriteLessonsDetail from '../screens/FavoriteLessonsDetail';

class NavigationDrawerStructure extends Component {
    //Structure for the navigatin Drawer
    toggleDrawer = () => {
      //Props to open/close the drawer
      this.props.navigationProps.toggleDrawer();
    };
    render() {
      return (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
            {/*Donute Button Image */}
            <Image
              source={require('../assets/images/drawer.png')}
              style={{ width: 25, height: 25, marginLeft: 5 }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }

export default createStackNavigator({
  FavoriteLessons: {
      screen: FavoriteLessons,
      navigationOptions: ({ navigation }) => ({
        title: 'Deep English',
        headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#2a5477',
        },
        headerTintColor: '#fff',
      }),
    },
    FavoriteLessonsDetail: {
      screen: FavoriteLessonsDetail
    },
    FreeLessonDetail: {
      screen: FreeLessonDetail
    }
	}
);