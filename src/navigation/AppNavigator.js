import { createDrawerNavigator, createAppContainer } from 'react-navigation';

import FreeLesson_StackNavigator from './FreeLessonNavigator';
import MembersLesson_StackNavigator from './MembersNavigator';
import FavoritesLesson_StackNavigator from './FavoriteNavigator';
import AuthNavigator from './AuthNavigator';
import WeeklyCourseScreen from '../screens/WeeklyCourseScreen';
import LogInScreen from '../screens/LogInScreen';
  
const AppDrawerNavigator = createDrawerNavigator({
  
  FreeLesson: {
    //Title
    screen: FreeLesson_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Free Lessons'.toUpperCase(),
    },
  },
  Memebers: {
    //Title
    screen: MembersLesson_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Memebers'.toUpperCase(),
    },
  },
  // 7-Day course
  WeeklyCourse: {
    screen: WeeklyCourseScreen,
    navigationOptions: {
      drawerLabel: '7-Day Course'.toUpperCase(),
    }
  },
  Favorite: {
    //Title
    screen: FavoritesLesson_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Favorites'.toUpperCase(),
    },
  },
  LogInScreen: {
    screen: AuthNavigator,
    navigationOptions: {
      drawerLabel: 'Log in'.toUpperCase(),
    }
  }
  // Close button
  // login / log out button
}, {
  contentOptions: {
    inactiveTintColor: '#999999',
    activeTintColor: '#999999',
    inactiveBackgroundColor: '#fff', 
    activeBackgroundColor: '#fff',
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1
    }
  }
});
 
export default createAppContainer(AppDrawerNavigator);
