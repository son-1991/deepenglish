import React, {Component} from 'react';
import { createStackNavigator } from 'react-navigation';
import { View, Image, TouchableOpacity } from 'react-native';

import MembersPlan from '../screens/MembersPlan';
import MembersProduct from '../screens/MembersProduct';
import MembersCollection from '../screens/MembersCollection';
import MembersLesson from '../screens/MembersLesson';

import LessonSet from '../screens/LessonSet';

class NavigationDrawerStructure extends Component {
    //Structure for the navigatin Drawer
    toggleDrawer = () => {
      //Props to open/close the drawer
      this.props.navigationProps.toggleDrawer();
    };
    render() {
      return (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
            {/*Donute Button Image */}
            <Image
              source={require('../assets/images/drawer.png')}
              style={{ width: 25, height: 25, marginLeft: 5 }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }
   
export default createStackNavigator({
    MembersPlan: {
      screen: MembersPlan,
      navigationOptions: ({ navigation }) => ({
        title: 'Deep English',
        headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#2a5477',
        },
        headerTintColor: '#fff',
      }),
    },
    MembersProduct: {
        screen: MembersProduct,
        navigationOptions: ({ navigation }) => ({
          title: 'Deep English',
          headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
          headerStyle: {
            backgroundColor: '#2a5477',
          },
          headerTintColor: '#fff',
        }),
    },
    MembersCollection: {
        screen: MembersCollection,
        navigationOptions: ({ navigation }) => ({
          title: 'Deep English',
          headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
          headerStyle: {
            backgroundColor: '#2a5477',
          },
          headerTintColor: '#fff',
        }),
    },
    LessonSet: {
        screen: LessonSet,
        navigationOptions: ({ navigation }) => ({
            title: 'Deep English',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
            headerStyle: {
              backgroundColor: '#2a5477',
            },
            headerTintColor: '#fff',
        }),
    },
    MembersLesson: {
        screen: MembersLesson,
        navigationOptions: ({ navigation }) => ({
            title: 'Deep English',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
            headerStyle: {
              backgroundColor: '#2a5477',
            },
            headerTintColor: '#fff',
        }),
    }
  });