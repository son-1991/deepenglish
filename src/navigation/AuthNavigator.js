import React, {Component} from 'react';
import { createStackNavigator } from 'react-navigation';
import { View, Image, TouchableOpacity } from 'react-native';

import LogInScreen  from '../screens/LogInScreen';
import FreeLesson from '../screens/FreeLessons';

class NavigationDrawerStructure extends Component {
    //Structure for the navigatin Drawer
    toggleDrawer = () => {
      //Props to open/close the drawer
      this.props.navigationProps.toggleDrawer();
    };
    componentDidMount() {
    }
    render() {
      return (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
            {/*Donute Button Image */}
            <Image
              source={require('../assets/images/drawer.png')}
              style={{ width: 25, height: 25, marginLeft: 5 }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }

export default createStackNavigator({
    LogInScreen: {
      screen: LogInScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Deep English',
        headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#2a5477',
        },
        headerTintColor: '#fff',
      }),
    },
    FreeLesson: {
        screen: FreeLesson,
        navigationOptions: ({ navigation }) => ({
            title: 'Deep English',
            headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
            headerStyle: {
              backgroundColor: '#2a5477',
            },
            headerTintColor: '#fff',
          }),
    }
});