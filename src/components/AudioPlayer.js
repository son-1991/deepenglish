import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Text, ScrollView, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from 'react-native-button';
import Slider from 'react-native-slider';
import Sound from 'react-native-sound';

const width = Dimensions.get('window').width;

const backward = require('../assets/images/fast-backward.png');
const forward = require('../assets/images/fast-forward.png');

export default class AudioPlayer extends Component {
 
    constructor(props) {
        super(props);
        Sound.setCategory('Playback');
    }

    lsAudioPlayer = null;

    state = {
        isPlaying: false,
        audioDuration: 0,
        currentTime: 0,
        offsetTime: 30,
        isLoading: false,
    }

    componentDidMount() {
        this.loadAudioFile();
    }

    loadAudioFile = async () => {
        this.setState({isLoading: true});
        audioUrl = this.props.free ? 'https://deepenglish.com/audio/'+ this.props.uri : this.props.uri;
        console.log('this-props->uri', audioUrl)
        this.lsAudioPlayer = new Sound( audioUrl, '', (error) => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + this.lsAudioPlayer.getDuration() + 'number of channels: ' + this.lsAudioPlayer.getNumberOfChannels());
            this.setState({audioDuration: this.lsAudioPlayer.getDuration()})
            
            // Play the sound with an onEnd callback
            // this.lsAudioPlayer.play((success) => {
            // if (success) {
            //   console.log('successfully finished playing');
            // } else {
            //   console.log('playback failed due to audio decoding errors');
            // }
        });
        this.lsAudioPlayer.setNumberOfLoops(-1);
        this.setState({isLoading: false});
    }

    onPlayPause = async () => {
        if(this.state.isLoading) return;
        if(!this.state.isPlaying) {
            this.audioTimer = setInterval( () => this.audioPlayerTimer() , 500);
        } else {
            clearInterval(this.audioTimer);
        }
        this.state.isPlaying ? this.lsAudioPlayer.pause() : this.lsAudioPlayer.play();
        console.log('isPlaying--', this.state.isPlaying);
        this.setState({isPlaying: !this.state.isPlaying});
    }

    audioPlayerTimer = async () => {
        this.setState({currentTime: this.state.currentTime + 0.5})
        if(this.state.currentTime > this.state.audioDuration) {
            this.lsAudioPlayer.stop();
            this.lsAudioPlayer.play();
            this.setState({currentTime: 0});
        }
    }

    onBackward = async () => {
        if(this.state.isLoading || !this.state.isPlaying) return;
        this.lsAudioPlayer.getCurrentTime((time) => {
            console.log('backward; current-time ---', time)
            time = (time - this.state.offsetTime < 0) ? 0 : (time - this.state.offsetTime);
            console.log('backwarded time', time);
            this.lsAudioPlayer.setCurrentTime(time);
            this.setState({currentTime: time});
        });        
    }

    onForward = async () => {
        if(this.state.isLoading || !this.state.isPlaying) return;
        this.lsAudioPlayer.getCurrentTime((time) => {
            console.log('forward: current-time ---', time);
            time = (time + this.state.offsetTime > this.state.audioDuration) ? this.state.audioDuration : (time + this.state.offsetTime);
            console.log('forwarded time', time);
            this.lsAudioPlayer.setCurrentTime(time);
            this.setState({currentTime: time});
        });
    }

    render() {
        return(
            <View style={{flex: 1, flexDirection: 'row'}}>
                <Button 
                    containerStyle={{backgroundColor: '#2A5477', width: width/6 - 16, height: width/9 - 16, borderRadius: 1, margin: 8, justifyContent: 'center', alignItems: 'center'}}
                    onPress={() => this.onPlayPause()} >
                    <Text style={{fontSize: 13, color: '#fff'}}>{this.state.isPlaying ? 'Pause' : 'Play'}</Text>
                </Button>

                <Slider
                    value={this.state.currentTime }
                    onSlidingComplete={ (currentTime) => {
                        if(!this.state.isLoading) {
                            console.log('current-time-slider', currentTime);
                            this.setState({ currentTime: currentTime });
                            this.lsAudioPlayer.setCurrentTime(currentTime);
                        }
                    }}
                    step={1}
                    minimumValue={0}
                    maximumValue={this.state.audioDuration / 5 * 5}
                    minimumTrackTintColor='#4f8ab6'
                    maximumTrackTintColor='#999999'
                    thumbTintColor='#f1c40f'
                    style={ this.props.free ? {width: width * 2 / 5} : {width: width * 3 / 5}}
                    trackStyle={{height:2}}
                    thumbStyle={{height:10, width:10}}
                    // onSlidingComplete = {() => { console.log('completed'); this.setState({currentTime: 0})}}
                />

                <TouchableOpacity onPress={() => this.onBackward()}>
                    <Image style={{margin: 10, width: width/18, height: width/18}} source={backward}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.onForward()}>
                    <Image style={{margin: 10, width: width/18, height: width/18}} source={forward}/>
                </TouchableOpacity>
            </View>
        );
    }
}