/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createStackNavigator, createAppContainer } from 'react-navigation';

import SplashScreen from './src/screens/SplashScreen';
import AppNavigator from './src/navigation/AppNavigator';

console.disableYellowBox = true;

const RootStack = createStackNavigator({
  SplashScreen: { screen: SplashScreen },
  AppNavigator: { screen: AppNavigator },
}, {
    initialRouteName: 'SplashScreen',
    headerMode: 'null',
  })

const AppContainer = createAppContainer(RootStack);

export default AppContainer;